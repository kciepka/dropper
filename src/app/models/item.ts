import { ItemType } from './item-type';

export class Item{
    name: string;
    creationDate: Date;
    type: ItemType;
    size: number;
    url: string;
    thumbnail: string;

    constructor(){
        this.creationDate = new Date();
    }

    parse(data){
        this.name = data.name;
        this.creationDate = data.creationDate;
        this.type = data.type;
        this.size = data.size;
        this.url = data.url;
        this.thumbnail = data.thumbnail;
    }
}