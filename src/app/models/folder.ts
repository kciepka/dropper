import { Item } from './item';
import { ItemType } from './item-type';
import { error } from 'util';

export class Folder extends Item {
    constructor(){
        super();
        this.type = ItemType.FOLDER;
    }

    parse(data){
        if(data.type != ItemType.FOLDER){
            throw new Error("Incorrect item type: " + data.type);
        }

        super.parse(data);
    }
}