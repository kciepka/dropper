import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';

import { AppComponent } from './app.component';
import { ItemComponent } from './controls/item/item.component';
import { FolderComponent } from './controls/folder/folder.component';
import { FileComponent } from './controls/file/file.component';

import { ErrorService } from './services/error.service';


@NgModule({
  declarations: [
    AppComponent,
    ItemComponent,
    FolderComponent,
    FileComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule
  ],
  providers: [
    {
      provide: ErrorHandler, 
      useClass: ErrorService
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
