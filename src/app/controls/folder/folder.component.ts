import { Component, OnInit } from '@angular/core';
import { ItemComponent } from '../item/item.component';

@Component({
  selector: 'app-folder',
  templateUrl: './folder.component.html',
  styleUrls: ['./folder.component.css']
})
export class FolderComponent extends ItemComponent implements OnInit {

  constructor() {
    super();
  }

  ngOnInit() {
  }
}
