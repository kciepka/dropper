import { Component, OnInit, Input } from '@angular/core';
import { Item } from '../../models/item';
import { ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ItemComponent implements OnInit {

  @Input() data : Item;
  
  constructor() {}

  ngOnInit() {}
}
